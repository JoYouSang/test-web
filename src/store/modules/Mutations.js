export default {
    authRequest(state) {
        state.status = 'pending'
    },
    authSuccess(state, user) {
        state.status = "success",
        state.me = user
    },
    authFail(state){
        clearState(state, 'fail')
    },
    logout(state){
        clearState(state, 'logout')
    },
    addSample(state, sample) {
        state.samples.push(sample);
    }
}

const clearState = (state, status) => {
    state.status = status
    state.token = ''
    state.tokenType = ''
    state.me = {}
}