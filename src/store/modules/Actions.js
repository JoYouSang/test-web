export default {
    async login({commit}, {id, pw, samples}) {
        commit('authRequest');

        let loginChk = false;
        for(let i=0; i<samples.length; i++){
            if(samples[i].id === id && samples[i].pw === pw){
                commit('authSuccess', samples[i]);
                loginChk = true;
                break;
            }
        }

        if(!loginChk) commit('authFail');
        return loginChk;
    },
    logout({commit}){
        commit('logout');
    },
    async addSample({commit}, {id, pw, name}) {
        const sample = {id, pw, name}
        commit("addSample", sample);

        return true;
    }
}