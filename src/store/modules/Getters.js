export default {
    getSamples: state => state.samples,
    getStatus: state => state.status,
    getMe: state => state.me
}