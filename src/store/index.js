import Vue from "vue";
import Vuex from "vuex";
import getters from './modules/Getters'
import actions from './modules/Actions'
import mutations from './modules/Mutations'

Vue.use(Vuex);

const samples = [
  {id: "admin", pw:"admin", name:"관리자"},
  {id: "test1", pw:"test1", name:"아이언맨"},
  {id: "test2", pw:"test2", name:"홍길동"},
  {id: "test3", pw:"test3", name:"김유신"}
]

const state = {
  status: '',
  token: '',
  tokenType: '',
  socketConnected: false,
  subscribeList: {},
  wsData: {},
  me: {},
  samples
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
});
